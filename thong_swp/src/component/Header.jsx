import React from 'react'
function Header() {
  return (
    <>
          <header className="header-area header-sticky">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <nav className="main-nav">
                                <a href="index.html" className="logo">
                                    <h1>MinjiLab</h1>
                                </a>
                                <ul className="nav">
                                    <li><a href="/about" className="active">Về MinjiLab</a></li>
                                    <li><a href="">Kim Cương</a></li>
                                    <li><a href="/service">Dịch Vụ</a></li>
                                    <li><a href="/lookup">Tra Cứu</a></li>
                                    <li><a href="">Contact Us</a></li>
                                    <li><a href="/login"><i className="fa fa-calendar" /> Login / Đăng Nhập</a></li>
                                </ul>
                                <a className="menu-trigger">
                                    <span>Menu</span>
                                </a>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>
    </>
  )
}

export default Header