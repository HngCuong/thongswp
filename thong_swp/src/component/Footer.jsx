import React from 'react'
import img1 from '../assets/images/appstore.png'
import img from '../assets/images/googleplay.png'
function Footer() {
  return (
    <>
    <div className="footer-29 py-5">
          <div className="container py-md-4">
            <div className="row footer-top-29">
              <div className="col-lg-4 col-md-6 col-sm-7 footer-list-29 footer-1 pr-lg-5">
                <h6 className="footer-title-29">Contact Info </h6>
                <p>
                  Address : Study course, 343 marketing, #2214 cravel street, NY
                  - 62617.
                </p>
                <p className="my-2">
                  Phone : <a href="tel:+1(21) 234 4567">+1(21) 234 4567</a>
                </p>
                <p>
                  Email : <a href="mailto:info@example.com">info@example.com</a>
                </p>
                <div className="main-social-footer-29 mt-4">
                  <a href="" className="facebook">
                    <span className="fa fa-facebook"></span>
                  </a>
                  <a href="" className="twitter">
                    <span className="fa fa-twitter"></span>
                  </a>
                  <a href="" className="instagram">
                    <span className="fa fa-instagram"></span>
                  </a>
                  <a href="" className="linkedin">
                    <span className="fa fa-linkedin"></span>
                  </a>
                </div>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-5 col-6 footer-list-29 footer-2 mt-sm-0 mt-5">
                <ul>
                  <h6 className="footer-title-29">Company</h6>
                  <li>
                    <a href="">About company</a>
                  </li>
                  <li>
                    <a href=""> Latest Blog posts</a>
                  </li>
                  <li>
                    <a href=""> Became a teacher </a>
                  </li>
                  <li>
                    <a href="">Online Courses</a>
                  </li>
                  <li>
                    <a href="">Get in touch</a>
                  </li>
                </ul>
              </div>
              <div className="col-lg-2 col-md-6 col-sm-5 col-6 footer-list-29 footer-3 mt-lg-0 mt-5">
                <h6 className="footer-title-29">Programs</h6>
                <ul>
                  <li>
                    <a href="">Documentation</a>
                  </li>
                  <li>
                    <a href="">Release Status</a>
                  </li>
                  <li>
                    <a href=""> Customers</a>
                  </li>
                  <li>
                    <a href=""> Help Center</a>
                  </li>
                </ul>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-7 footer-list-29 footer-4 mt-lg-0 mt-5">
                <h6 className="footer-title-29">Suppport</h6>
                <a href="">
                  <img
                     src={img}
                    className="img-responsive"
                    alt=""
                  />
                </a>
                <a href="">
                  <img
                    src={img1}
                    className="img-responsive mt-3"
                    alt=""
                  />
                </a>
              </div>
            </div>
          </div>
        </div>
    </>
  )
}

export default Footer