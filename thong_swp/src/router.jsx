import { createBrowserRouter, Outlet, Navigate } from 'react-router-dom';
import { RootLayout } from './layout/RootLayout/RootLayout'; 
import About from './pages/About';
import Service from './pages/Service';
import Lookup from './pages/Lookup';
import { AuthLayout } from './layout/AuthLayout/AuthLayout';
import Login from './pages/Login';

export const router = createBrowserRouter([
  {
    element: <ContextWrapper />,
    children: [
      {
        path: '*',
        element: (
          <Navigate
            to='/about'
            replace
          />
        ),
      },
      {
        element: <RootLayout />,
        children: [
          { path: 'about', element: <About /> },
          { path: 'service', element: <Service /> },
          { path: 'lookup', element: <Lookup /> },
        ],
      },
      {
        element: <AuthLayout />,
        children: [
          { path: 'login', element: <Login /> },
        ],
      },
        ],
      },
]);

function ContextWrapper() {
  return (
        <Outlet />
  );
}
