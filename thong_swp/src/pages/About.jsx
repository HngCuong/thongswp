import '../style-starter.css'
import '../App.css'
import 'owl.carousel/dist/assets/owl.carousel.css'; // Import CSS
import 'owl.carousel';
import React, { useEffect } from 'react';
function About() {
    useEffect(() => {
        // Initialize Owl Carousel
        $('.owl-one').owlCarousel({
            loop: true,
            margin: 0,
            nav: false,
            dots: false,
            responsiveClass: true,
            autoplay: true,
            autoplayTimeout: 5000,
            autoplaySpeed: 1000,
            autoplayHoverPause: false,
            responsive: {
                0: {
                    items: 1,
                },
                480: {
                    items: 1,
                },
                667: {
                    items: 1,
                },
                1000: {
                    items: 1,
                    nav: true,
                },
            },
        });
    }, []);
    return (
        <>
            <section class="w3l-main-slider" id="home">
                <div class="companies20-content">
                    <div className="owl-one owl-carousel owl-theme">
                        <div className="item">
                            <li>
                                <div className="slider-info banner-view bg bg2">
                                    <div className="banner-info">
                                        <div className="container">
                                            <div className="banner-info-bg">
                                                <h5>50% Discount on all Popular Courses</h5>
                                                <p className="mt-4 pr-lg-4">
                                                    Take the first step to your journey to success with us
                                                </p>
                                                <a className="btn btn-style btn-primary mt-sm-5 mt-4 mr-2" >
                                                    Ready to  get started?
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </div>
                        <div className="item">
                            <li>
                                <div className="slider-info  banner-view banner-top1 bg bg2">
                                    <div className="banner-info">
                                        <div className="container">
                                            <div className="banner-info-bg">
                                                <h5>Learn and Improve Yourself in Less Time </h5>
                                                <p className="mt-4 pr-lg-4">
                                                    Our self improvement courses is very effective
                                                </p>
                                                <a className="btn btn-style btn-primary mt-sm-5 mt-4 mr-2" >
                                                    Ready to get started?
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </div>
                        <div className="item">
                            <li>
                                <div className="slider-info banner-view banner-top2 bg bg2">
                                    <div className="banner-info">
                                        <div className="container">
                                            <div className="banner-info-bg">
                                                <h5>Be More Productive to Be More Successful</h5>
                                                <p className="mt-4 pr-lg-4">
                                                    Don't waste your time, check out our productive courses
                                                </p>
                                                <a className="btn btn-style btn-primary mt-sm-5 mt-4 mr-2" >
                                                    Ready to get started?
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </div>
                        <div className="item">
                            <li>
                                <div className="slider-info banner-view banner-top3 bg bg2">
                                    <div className="banner-info">
                                        <div className="container">
                                            <div className="banner-info-bg">
                                                <h5>Enhance your skills with best online courses</h5>
                                                <p className="mt-4 pr-lg-4">
                                                    Take the first step to your journey to success with us
                                                </p>
                                                <a className="btn btn-style btn-primary mt-sm-5 mt-4 mr-2">
                                                    Ready to get started?
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </div>
                        {/* Add more items as needed */}
                    </div>
                </div>

                <div class="waveWrapper waveAnimation">
                    <svg viewBox="0 0 500 150" preserveAspectRatio="none">
                        <path d="M-5.07,73.52 C149.99,150.00 299.66,-102.13 500.00,49.98 L500.00,150.00 L0.00,150.00 Z"
                            style={{ stroke: 'none' }}>
                        </path>
                    </svg>
                </div>
            </section>
            <section className="w3l-features py-5" id="facilities">
                <div className="call-w3 py-lg-5 py-md-4 py-2">
                    <div className="container">
                        <div className="row main-cont-wthree-2">
                            <div className="col-lg-5 feature-grid-left">
                                <h5 className="title-small mb-1">Introduction and Overview</h5>
                                <h3 className="title-big mb-4">About our company</h3>
                                <p className="text-para">The establishment of Minji Laboratory Company Limited (MinjiL) marks a significant milestone in the development strategy of  Company in particular, and the Jewelry industry in general. It reaffirms the quality of Minji's gemstone products and contributes to building a National Brand of international caliber.

                                    MinjiL's predecessor, was founded in 1996 Company. Over the years, it has operated in the field of appraisal services with a team of experienced appraisal experts and the most modern technological equipment. MinjiL's appraisal standards are synthesized based on the best appraisal standards of gemstone institutes worldwide. </p>

                                <a href="#url" className="btn btn-primary btn-style mt-md-5 mt-4">Discover More</a>
                            </div>
                            <div className="col-lg-7 feature-grid-right mt-lg-0 mt-5">
                                <div className="call-grids-w3 d-grid">
                                    <div className="grids-1 box-wrap">
                                        <a href="#more" className="icon"><span className="fa fa-certificate"></span></a>
                                        <h4><a href="#feature" className="title-head">Diamond Assessment and Consulting.</a></h4>
                                        <p>Vivamus a ligula quam. Ut blandit eu leo non. Duis sed doloramet laoreet.</p>
                                    </div>
                                    <div className="grids-1 box-wrap">
                                        <a href="#more" className="icon"><span className="fa fa-book"></span></a>
                                        <h4><a href="#feature" className="title-head">Assessment and Consulting on Precious and Semi-Precious Stones.</a></h4>
                                        <p>Vivamus a ligula quam. Ut blandit eu leo non. Duis sed dolor amet laoreet.</p>
                                    </div>
                                    <div className="grids-1 box-wrap">
                                        <a href="#more" className="icon"><span className="fa fa-trophy"></span></a>
                                        <h4><a href="#feature" className="title-head">Assessment and Consulting on Gold, Silver, Precious Metals, Non-ferrous Metals.</a></h4>
                                        <p>Vivamus a ligula quam. Ut blandit eu leo non. Duis sed dolor amet laoreet.</p>
                                    </div>
                                    <div className="grids-1 box-wrap">
                                        <a href="#more" className="icon"><span className="fa fa-graduation-cap"></span></a>
                                        <h4><a href="#feature" className="title-head">Online Payment</a></h4>
                                        <p>Vivamus a ligula quam. Ut blandit eu leo non. Duis sed dolor amet laoreet.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section className="w3l-footer-29-main">
            </section>
        </>
    )
}

export default About
