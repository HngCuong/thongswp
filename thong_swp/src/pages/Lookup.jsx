
import '../style-starter.css'
import '../App.css'
import 'owl.carousel/dist/assets/owl.carousel.css'; // Import CSS
import 'owl.carousel';
import React from 'react';
import { Box, Typography, Input } from '@mui/material';
function Lookup() {
  return (
    <>
      <Box sx={{ width: '100%' }}>
        <Typography sx={{ textAlign: 'center', justifyContent: 'center', color: 'blue', fontSize: '30px' }}>
          Tra cứu thông tin sản phẩm
        </Typography>
        <Typography sx={{ textAlign: 'center', justifyContent: 'center', color: 'black', fontSize: '10px', display: 'block' }}>
          Đây là dịch vụ tra cứu của PNJL nhằm mục đích mang đến cho quý khách hàng sự tiện lợi, nhanh chóng và chính xác về những thông tin của sản phẩm trên bảng giám định đúng với những thông tin lưu trong cơ sở dữ liệu của PNJL
        </Typography>

        <Box
          bgcolor='#F8F0DE'
          sx={{
            alignItems: 'center',
            justifyContent: 'center',
            height: '300px',
            width: '500px',
            marginLeft: '380px'
          }}>
          <Typography sx={{ marginLeft: '20px', justifyContent: 'center', color: 'black', fontSize: '20px', display: 'block' }}>
            Kim Cương
          </Typography>
          <Typography sx={{ marginLeft: '20px', justifyContent: 'center', color: 'black', fontSize: '20px', display: 'block' }}>
            Số Sản Phẩm
          </Typography>
          <Box>
          <Input
            sx={{marginLeft: '20px',}}
            size="lg"
            placeholder="Nhập số sản phẩm"
            className="border border-black rounded-lg p-2  bg-white"
            labelProps={{
           
            }}
          /> 
          <button className="ml-5 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
            Truy Xuất
          </button>
          </Box>
         
          <Typography sx={{ marginLeft: '20px', color: 'black', fontSize: '10px', display: 'block' }}>
            LƯU Ý QUAN TRỌNG: Bảng tham khảo này chỉ nhằm mục đích giúp xác nhận số kiểm định (Number) do người sử dụng dịch vụ này cung cấp tương ứng với số kiểm định được lưu trữ tại nguồn dữ liệu của PNJL. Bảng tham khảo này không phải là giấy bảo hành, đánh giá hay định giá viên kim cương đính kèm và cũng không được xem như là một bảng giám định gốc của PNJL.
          </Typography>
        
        </Box>

      </Box>
      <section className="w3l-footer-29-main">
      </section>
    </>
  )
}

export default Lookup
