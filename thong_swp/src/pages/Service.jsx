
import '../style-starter.css'
import '../App.css'
import 'owl.carousel/dist/assets/owl.carousel.css'; // Import CSS
import 'owl.carousel';
import img from '../assets/images/googleplay.png'
import img1 from '../assets/images/appstore.png'
import React, { useEffect } from 'react';
import { Card, Box } from '@mui/material';
function Service() {
  const authorsTableData = [
    {
      img: "path/to/image1.jpg",
      name: "John Doe",
      email: "john.doe@example.com",
      job: ["Designer", "UI/UX Designer"],
      online: true,
      date: "2024-05-22",
    },
    {
      img: "path/to/image2.jpg",
      name: "Jane Smith",
      email: "jane.smith@example.com",
      job: ["Developer", "Frontend Developer"],
      online: false,
      date: "2024-05-21",
    },
    // Add more data objects as needed
  ];
  return (
    <>
      <Box>
        <div className="flex">
          <p className="ml-5 text-blue-900 font-semibold text-2xl mb-4 flex">DIAMOND APPRAISAL SERVICES AVAILABLE AT MinjiLAB</p>
          <button className="ml-5 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
            Click me
          </button>
        </div>
        <div className="flex">
          <table className="w-[50vw] ml-4 table-auto border border-collapse mb-5">
            <thead>
              <tr>
                {["STT", "LOẠI DỊCH VỤ", "NỘI DUNG", "GiÁ TIỀN", "KÍCH THƯỚC"].map((el) => (
                  <th
                    key={el}
                    className="border-b border-blue-gray-50 py-3 px-4 text-left"
                  >
                    <p
                      className="text-[11px] font-bold uppercase text-blue-gray-400"
                    >
                      {el}
                    </p>
                  </th>
                ))}
              </tr>
            </thead>
            <tbody>
              {authorsTableData.map(
                ({ name, email, job, date }, key) => {
                  const className = `py-3 px-4 ${key === authorsTableData.length - 1
                      ? ""
                      : "border-b border-blue-gray-50"
                    }`;

                  return (
                    <tr key={name}>
                      <td className={className}>
                        <div className="flex items-center gap-4">
                          <div>
                            <p
                              variant="small"
                              color="blue-gray"
                              className="font-semibold"
                            >
                              {name}
                            </p>
                            <p className="text-xs font-normal text-blue-gray-500">
                              {email}
                            </p>
                          </div>
                        </div>
                      </td>
                      <td className={className}>
                        <p className="text-xs font-semibold text-blue-gray-600">
                          {job[0]}
                        </p>
                        <p className="text-xs font-normal text-blue-gray-500">
                          {job[1]}
                        </p>
                      </td>
                      <td className={className}>
                        <p className="text-xs font-semibold text-blue-gray-600">
                          {date}
                        </p>
                      </td>
                      <td className={className}>
                        <p className="text-xs font-semibold text-blue-gray-600">
                          {date}
                        </p>
                      </td>
                      <td className={className}>
                        <p
                          className="text-xs font-semibold text-blue-gray-600"
                        >
                          Edit
                        </p>
                      </td>
                    </tr>
                  );
                }
              )}
            </tbody>
          </table>
          <img src={img} alt="Image" className="img-button" sx={{ height: '50px !important', width: '50px !important' }} />
        </div>
      </Box>

      <Card>
        <p className="ml-5 text-blue-900 font-semibold text-2xl mb-4">DIAMOND APPRAISAL SERVICES AVAILABLE AT MinjiLAB</p>
        <table className="w-[50vw] ml-4 table-auto border border-collapse mb-5 ">
          <thead>
            <tr>
              {["STT", "LOẠI DỊCH VỤ", "NỘI DUNG", "GiÁ TIỀN", "KÍCH THƯỚC"].map((el) => (
                <th
                  key={el}
                  className="border-b border-blue-gray-50 py-3 px-4 text-left"
                >
                  <p
                    className="text-[11px] font-bold uppercase text-blue-gray-400"
                  >
                    {el}
                  </p>
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {authorsTableData.map(
              ({ name, email, job, date }, key) => {
                const className = `py-3 px-4 ${key === authorsTableData.length - 1
                    ? ""
                    : "border-b border-blue-gray-50"
                  }`;

                return (
                  <tr key={name}>
                    <td className={className}>
                      <div className="flex items-center gap-4">
                        <div>
                          <p
                            variant="small"
                            color="blue-gray"
                            className="font-semibold"
                          >
                            {name}
                          </p>
                          <p className="text-xs font-normal text-blue-gray-500">
                            {email}
                          </p>
                        </div>
                      </div>
                    </td>
                    <td className={className}>
                      <p className="text-xs font-semibold text-blue-gray-600">
                        {job[0]}
                      </p>
                      <p className="text-xs font-normal text-blue-gray-500">
                        {job[1]}
                      </p>
                    </td>
                    <td className={className}>
                      <p className="text-xs font-semibold text-blue-gray-600">
                        {date}
                      </p>
                    </td>
                    <td className={className}>
                      <p className="text-xs font-semibold text-blue-gray-600">
                        {date}
                      </p>
                    </td>
                    <td className={className}>
                      <p
                        className="text-xs font-semibold text-blue-gray-600"
                      >
                        Edit
                      </p>
                    </td>
                  </tr>
                );
              }
            )}
          </tbody>
        </table>
      </Card>
    </>
  )
}

export default Service
