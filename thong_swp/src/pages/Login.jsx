import React from 'react'
import './Login.css'
import { FaLock, FaUser } from 'react-icons/fa';
import {
  Input,
  Checkbox,
  Button,
  Typography,
} from "@mui/material";
import img from "../assets/images/appstore.png"
const Login = () => {
  return (
    <div className='login'>

    <div className='wrapper'>

      <form>

        <h1>Login</h1>

        <div className='input-box'>

          <input type='text' placeholder='Username' required  />

          <FaUser className='icon' />
        
        </div>

        <div className='input-box'>

          <input type='password' placeholder='Password' required  />

          <FaLock className='icon' />

        </div>

        <div className='remember-forgot'>

          <label><input type='checkbox' />Remember me</label>

          <a href='a'>Forgot password</a>

        </div>

        <button>Login</button>

        <div className='register-link'>

          <p>Don't have an account ? <a href='a' />Register</p>

        </div>
       

      </form>

    </div>

  </div>

  );
}

export default Login;