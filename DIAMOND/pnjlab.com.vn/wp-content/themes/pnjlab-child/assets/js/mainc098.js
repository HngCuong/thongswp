jQuery(document).ready(function ($) {
	
		function half_container() {
			$('.home-section-1 .large-4').each(function(){
				var containerLeft = $('.home-section-1 .large-4').offset().left + 'px';
        var sectionTop = ($('.home-section-1 .large-4').offset().top - $('.home-section-1').offset().top - 35) + 'px';
        
        
				$('.home-section-1').css({
            "--left-var": containerLeft, 
            "--top-var": sectionTop
          });
			});
		}
		half_container();
		$(window).on('resize', function () {
			half_container();
		});

});

// onrendered: function(canvas) {
// 		  //   var  _canvas = document.createElement("canvas");
// 		  //   _canvas.setAttribute('width', 1080);
// 		  //   _canvas.setAttribute('height', 1920);
// 		  //   var ctx = _canvas.getContext('2d');
// 		  //   ctx.drawImage(canvas, 0, 0, canvas.width, canvas.height, 0, 0, 1080, 1920);
// 		  //   var dataURL = _canvas.toDataURL();  
// 		  //   document.getElementById("canvasWrapper").appendChild(_canvas);
// 		  //   var image = _canvas.toDataURL("image/png");
// 		  //   simulateDownloadImageClick(dataURL.toDataURL(), 'file-name.png');
// 		  // },
const el = document.getElementById('download_thanks');
if (el !== null) {
  setUpDownloadPageAsImage();
}
function setUpDownloadPageAsImage() {
  document.getElementById("download_thanks").addEventListener("click", function() {
    html2canvas(document.getElementById("contact-form-thank")).then(function(canvas) {
      console.log(canvas);
      simulateDownloadImageClick(canvas.toDataURL(), 'dat-lich-giam-dinh.png');
    });
  });
}


function simulateDownloadImageClick(uri, filename) {
  var link = document.createElement('a');
  if (typeof link.download !== 'string') {
    window.open(uri);
  } else {
    link.href = uri;
    link.download = filename;
    accountForFirefox(clickLink, link);
  }
}

function clickLink(link) {
  link.click();
}

function accountForFirefox(click) { // wrapper function
  let link = arguments[1];
  document.body.appendChild(link);
  click(link);
  document.body.removeChild(link);
}