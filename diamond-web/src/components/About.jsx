import React, { useEffect } from 'react';
import Header from './Header'
import Footer from './Footer'

const About = () => {
    return (
        <>
            <Header />
            <section className="section" id="section_323910886">
      <div className="bg section-bg fill bg-fill bg-loaded"></div>
      <div className="section-content relative">
        <div className="row row-large align-center" id="row-787482710">
          <div id="col-1792328860" className="col medium-12 small-12 large-6">
            <div className="col-inner">
              <div className="container section-title-container" style={{ marginBottom: '0px' }}>
                <h2 className="section-title section-title-normal">
                  <b></b>
                  <span className="section-title-main">Giới thiệu</span>
                  <b></b>
                </h2>
              </div>
              <p style={{ textAlign: 'justify' }}>
                Sự ra đời của Công Ty TNHH Một Thành Viên Giám Định PNJ (PNJL) là một bước tiến quan trọng nằm trong chiến lược phát triển của Công Ty Cổ Phần Vàng Bạc Đá Quý Phú Nhuận nói riêng và ngành Kim Hoàn nói chung. Khẳng định chất lượng sản phẩm đá quý của PNJ và góp phần tạo dựng một Thương Hiệu Quốc Gia có chất lượng ngang tầm Quốc tế.
                <br />
                Tiền thân là Trung Tâm Giám Định PNJ được thành lập từ năm 1996, do Công Ty Cổ Phần Vàng Bạc Đá Quý Phú Nhuận làm chủ sở hữu. Qua nhiều năm hoạt động trong lĩnh vực dịch vụ giám định với đội ngũ chuyên gia giám định giàu kinh nghiệm, trang thiết bị máy móc công nghệ hiện đại nhất. Tiêu chuẩn giám định của PNJL được tổng hợp dựa trên những tiêu chuẩn giám định tốt nhất của các viện đá quý trên Thế Giới.
                <br />
                &#8211; Tên tiếng Việt: <span data-text-color="primary"><strong>Công Ty TNHH Một Thành Viên Giám Định PNJ</strong></span>
                <br />
                &#8211; Tên tiếng Anh: <span data-text-color="primary"><strong>PNJ Laboratory Company Limited</strong></span>
                <br />
                &#8211; Tên viết tắt: <span data-text-color="primary"><strong>PNJL Co., Ltd</strong></span>
              </p>
              <p>
                <img
                  decoding="async"
                  className="alignnone wp-image-1274 size-medium"
                  src="../../pnjlab.com.vn_443/wp-content/uploads/2022/12/LOGO-ISO-300x96.jpg"
                  alt=""
                  width="300"
                  height="96"
                  srcSet="https://pnjlab.com.vn/wp-content/uploads/2022/12/LOGO-ISO-300x96.jpg 300w, https://pnjlab.com.vn/wp-content/uploads/2022/12/LOGO-ISO.jpg 425w"
                  sizes="(max-width: 300px) 100vw, 300px"
                />
              </p>
            </div>
            <style>
              {`#col-1792328860 > .col-inner {
                  padding: 29px 0px 0px 0px;
                }`}
            </style>
          </div>
          <div id="col-1062793296" className="col medium-7 small-12 large-6 medium-col-first">
            <div className="col-inner">
              <div className="img has-hover has-bg right x md-x lg-x y md-y lg-y" id="image_1298025379">
                <div className="img-inner dark">
                  <img
                    width="545"
                    height="570"
                    src="../wp-content/uploads/2022/10/Mask-group.png"
                    className="attachment-large size-large"
                    alt=""
                    decoding="async"
                    loading="lazy"
                    srcSet="https://pnjlab.com.vn/wp-content/uploads/2022/10/Mask-group.png 545w, https://pnjlab.com.vn/wp-content/uploads/2022/10/Mask-group-287x300.png 287w"
                    sizes="(max-width: 545px) 100vw, 545px"
                  />
                </div>
                <style>
                  {`#image_1298025379 {
                      width: 100%;
                    }`}
                </style>
              </div>
            </div>
          </div>
        </div>
      </div>
      <style>
        {`#section_323910886 {
            padding-top: 42px;
            padding-bottom: 42px;
          }
          #section_323910886 .ux-shape-divider--top svg {
            height: 150px;
            --divider-top-width: 100%;
          }
          #section_323910886 .ux-shape-divider--bottom svg {
            height: 150px;
            --divider-width: 100%;
          }
          @media (min-width:850px) {
            #section_323910886 {
              padding-top: 75px;
              padding-bottom: 75px;
            }
          }`}
      </style>
    </section>

            <Footer />

        </>
    )
}

export default About