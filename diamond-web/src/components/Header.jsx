import React from 'react'

const Header = () => {
  return (
    <>
      {/* ***** Header Area Start ***** */}
      <header className="header-area header-sticky">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <nav className="main-nav">
                {/* ***** Logo Start ***** */}
                <a href="index.html" className="logo">
                  <h1>Villa</h1>
                </a>
                {/* ***** Logo End ***** */}
                {/* ***** Menu Start ***** */}
                <ul className="nav">
                  <li><a href="/about" className="active">About</a></li>
                  <li><a href="/service">Service</a></li>
                  <li><a href="/diamond-knowledge">Diamond Knowledge</a></li>
                  <li><a href="contact.html">Contact Us</a></li>
                  <li><a href="#"><i className="fa fa-calendar" /> Schedule an assessment</a></li>
                </ul>
                <a className="menu-trigger">
                  <span>Menu</span>
                </a>
                {/* ***** Menu End ***** */}
              </nav>
            </div>
          </div>
        </div>
      </header>
      {/* ***** Header Area End ***** */}


    </>
  )
}

export default Header