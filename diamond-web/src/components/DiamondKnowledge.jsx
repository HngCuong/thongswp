import React, { useEffect } from 'react';
import Header from './Header'
import Footer from './Footer' 

const DiamondKnowledge = () => {
    return ( 
        <>
        <Header /> 
        <div>
        <h1>The birth of PNJ Appraisal Company Limited (PNJL) is an important step in the development strategy of Phu Nhuan Jewelry Joint Stock Company in particular and the Jewelry industry in general. Affirming the quality of PNJ's gemstone products and contributing to building a National Brand with quality on par with international standards.
        Formerly known as PNJ Appraisal Center, it was established in 1996, owned by Phu Nhuan Jewelry Joint Stock Company. Through many years of operation in the field of inspection services with a team of experienced inspection experts and the most modern technological equipment and machinery. PNJL's appraisal standards are compiled based on the best appraisal standards of gemstone institutes in the world.
        – Vietnamese name: PNJ Appraisal Company Limited
        – English name: PNJ Laboratory Company Limited
        – Abbreviated name: PNJL Co., Ltd</h1>
        </div>

        <Footer />
        
        </>
    )
}

export default DiamondKnowledge