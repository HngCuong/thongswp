import logo from './logo.svg';
import './App.css';
import Home from './components/Home';
import { Navigate, Route, Routes } from 'react-router-dom';
import About from './components/About';
import Service from './components/Service';
import DiamondKnowledge from './components/DiamondKnowledge';

function App() {
  return (
    <>
      {/* ***** Preloader Start ***** */}
      <div id="js-preloader" className="js-preloader">
        <div className="preloader-inner">
          <span className="dot" />
          <div className="dots">
            <span />
            <span />
            <span />
          </div>
        </div>
      </div>


      <div className="App">
        <Routes>
          <Route path="/" element={<Navigate to="/home" />} />

          <Route path="/home" element={<Home />} />
          <Route path="/about" element={<About />} />
          <Route path="/service" element={<Service />} />
          <Route path="/diamond-knowledge" element={<DiamondKnowledge />} />
        </Routes>
      </div>
    </>

  );
}

export default App;
